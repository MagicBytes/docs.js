# <u>docs.js</u>
docs.js is a javascript documentation generator
which generates API documentations from javascript source code using jsdoc
into readable HTML pages

## usage:
```bash
> ./docs.js generate --src ${SourceDirectory} --out ${DocumentationDirectory}
```

## get help:
```bash
> ./docs.js --help
```

## usage in code comments:
```
/**
 *
 * @file: index.js
 * @author: Pascal Peinecke
 *
*/

/**
 * @name setSth
 * @description Set some var
 * @param {int} i - First var
 * @param {int} y - Second var
*/

/**
 * @name getSth
 * @description Get some var
 * @return {int} z - result
*/
```

## sample documentation:
live version: https://pascal3366.github.io/apex-projekt


![](http://i.imgur.com/0wk2Inq.png?raw=true)

![](http://i.imgur.com/m7BqJls.png?raw=true)
